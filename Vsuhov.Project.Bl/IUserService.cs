﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Project.ViewModels;

namespace Vsuhov.Project.Bl
{
    public interface IUserService
    {
        List<UserViewModel> GetUsers();
        UserViewModel GetUser(int id);
        int AddUser(UserViewModelAdd model);
        void RemoveUser(int id);
    }
}
