﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Project.Dal;
using Vsuhov.Project.ViewModels;

namespace Vsuhov.Project.Bl
{
    public class DetailsService : IDetailsService
    {
        public DetailsViewModel GetDetails(int id)
        {
            var details = new DetailsViewModel();
            var db = new Context();
            var item = db.GetDetails(id);
            details.Age = item.Age;
            details.Id = item.Id;
            details.Address = item.Address;

            return details;
        }
    }
}
