﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vsuhov.Project.Dal;
using Vsuhov.Project.ViewModels;

namespace Vsuhov.Project.Bl
{
    public class UserService : IUserService
    {
        public UserViewModel GetUser(int id)
        {
            var user = new UserViewModel();
            var db = new Context();
            var user_item = db.GetUser(id);
            user.Id = user_item.Id;
            user.Name = user_item.Name;
            user.Surname = user_item.Surname;
            user.Details_id = user_item.Details_id;
            return user;
            
        }

        public List<UserViewModel> GetUsers()
        {
           var users =new List<UserViewModel>();
            var db = new Context();
            var items = from users_items in db.GetUsers()
                        select users_items;
            foreach (var item in items) {
                var user_item = new UserViewModel();
                user_item.Id = item.Id;
                user_item.Name = item.Name;
                user_item.Surname = item.Surname;
                user_item.Details_id = item.Details_id;
                users.Add(user_item);
            }

            return users;

        }

        public int AddUser(UserViewModelAdd user)
        {
            var users = new Users() { Name = user.Name, Surname = user.Surname };
            var details = new Details() { Age=user.Age, Address=user.Address };
            var db = new Context();
            return db.AddUser(users, details);
        }
        public void RemoveUser(int id)
        {
            var db = new Context();
            db.RemoveUser(id);
        }
    }
}
