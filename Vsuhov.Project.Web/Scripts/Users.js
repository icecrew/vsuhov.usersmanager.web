﻿$(document).ready(function () {
    $(".delete_user").click(function () {
        var id = $(this).attr('id');
        Users.deleteUser(id);
    });
    $(".details_user").click(function () {
        var id = $(this).attr('id');
        Users.getDetails(id);
    });
    $("table tr").mouseover(function () {
        $(this).children().css("background-color","grey");
    });
    $("table tr").mouseout(function () {
        $(this).children().css("background-color", "white");
    });
});

Users = {
    list: [],
    deleteUser: function (id) {
        if(confirm("Are you sure???"))
        $.ajax({
            type: "DELETE",
            url: "api/users/" + id,
            async: true,
            success: function (output, status, xhr) {
                location.reload();
            },
            error: function () {
                alert("Error occured!");
            }

        });

    },
    getDetails: function (id) {
        $.ajax({
            type: "GET",
            url: "api/details/" + id,
            async: true,
            success: function (output, status, xhr) {
                $(".user_details_output").empty();
                $('#detailsTemplate').tmpl(output).appendTo(".user_details_output");
                
            },
            error: function () {
                alert("Error accured!");
            }
        });
    }

}