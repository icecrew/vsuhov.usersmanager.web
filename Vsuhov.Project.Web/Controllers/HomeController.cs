﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vsuhov.Project.Bl;
using Vsuhov.Project.ViewModels;

namespace Vsuhov.Project.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var items = new UserService();
            return View(items.GetUsers());
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(UserViewModelAdd model)
        {
            var db = new UserService();
            db.AddUser(model);
            return RedirectToAction("Index");
        }
    }
}