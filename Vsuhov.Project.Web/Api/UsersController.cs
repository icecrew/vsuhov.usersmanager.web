﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vsuhov.Project.Bl;
using Vsuhov.Project.ViewModels;

namespace Vsuhov.Project.Web.Api
{
    public class UsersController : ApiController
    {
        // GET: api/users
        public IEnumerable<UserViewModel> Get()
        {
            var items = new UserService();
            return items.GetUsers();
        }

        // GET: api/users/id
        public UserViewModel Get(int id)
        {
            var items = new UserService();
            return items.GetUser(id);
        }

        // POST: api/users
        public int Post(UserViewModelAdd model)
        {
            var items = new UserService();
            return items.AddUser(model);
        }

        // PUT: api/Cars/5
        public void Put()
        {
            

        }

        // DELETE: api/Cars/5
        public void Delete(int id)
        {
            var items = new UserService();
            items.RemoveUser(id);
        }
    }
}