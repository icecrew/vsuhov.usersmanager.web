﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Vsuhov.Project.Dal;

namespace Vsuhov.Project.Dependency
{
    public class MyDependencyResolver : IDependencyResolver
    {
        public MyDependencyResolver()
        {
            container = new UnityContainer();
            container.RegisterType<IContext, Context>();
            container.RegisterType<IProductService, ProductService>();
        }
        public object GetService(Type serviceType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            throw new NotImplementedException();
        }
    }
}
