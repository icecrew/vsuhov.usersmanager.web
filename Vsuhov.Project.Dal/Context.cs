﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vsuhov.Project.Dal
{
    public class Context : IContext
    {
        public Details GetDetails(int id)
        {
            var result = new Details();
            using (var db = new UsersDBEntities())
            {
                var items = from detail in db.Details
                            where detail.Id == id
                            select detail;
                foreach (var item in items)
                {
                    result=item;
                }
            }
            return result;
        }

        public Users GetUser(int id)
        {
            var result = new Users();
            using (var db = new UsersDBEntities())
            {
                var items = from user in db.Users
                            where user.Id == id
                            select user;
                foreach (var item in items)
                {
                    result = item;
                }
            }
            return result;
        }

        public List<Users> GetUsers()
        {
            var result = new List<Users>();
            using (var db = new UsersDBEntities())
            {
                var items = from users in db.Users
                            select users;
                foreach (var item in items)
                {
                    result.Add(item);
                }
            }
            return result;
        }

        public int AddUser(Users user, Details detail)
        {
            using (var db = new UsersDBEntities())
            {
                db.Details.Add(detail);
                db.SaveChanges();
                user.Details_id = detail.Id;
                db.Users.Add(user);
                db.SaveChanges();
            }
            return user.Id;
        }
        public void RemoveUser(int id)
        {
            var result = new List<Users>();
            using (var db = new UsersDBEntities())
            {
                var user = (from item in db.Users
                           where item.Id == id
                           select item).First();
                db.Users.Remove(user);
                db.SaveChanges();
            }

        }
    }
}
