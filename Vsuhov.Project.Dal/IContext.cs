﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vsuhov.Project.Dal
{
    public interface IContext
    {
        List<Users> GetUsers();
        Users GetUser(int id);
        Details GetDetails(int id);
        void RemoveUser(int id);
    }
}
