﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vsuhov.Project.ViewModels
{
    public class DetailsViewModel
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }

    }
}
